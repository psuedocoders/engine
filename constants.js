/* *
 * Constants used
 *
 * By the Pseudocoders team:
 * Demetrios C, Jordan, James E.
 * */

const TOUCH_ON_PATH = '/touchOn';
const MAX_REQUEST_TIME = 30000; // 30 second timeout on the request
const PORT = 8086;

module.exports = {
    TOUCH_ON_PATH: TOUCH_ON_PATH,
    PORT: PORT
  };
