/*
 *Tests for each route in the engine. Uses the mockery module for
 *mocking external components
 * By the Pseudocoders team:
 * Demetrios C, Jordan, James E.
 */
var routes = require('../routes.js');
var express = require('../node_modules/express');
var timeout = require('../node_modules/timeout');
var firebase = require('../node_modules/firebase');
var app = express();
var constants = require('../constants');

module.exports = {
    'Test Post': function(test) {
        test.expect(1);
        test.equal(app.get('/', routes.homeGet()), 'GET RECEIVED');
        test.throws(function() { routes.homeGet(); });                  // no empty arguments
        test.throws(function() { routes.homeGet(null); });              // no null arg
        test.done();
    }
};
